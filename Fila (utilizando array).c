
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct que {
	int front, rear, size;
	unsigned actualSize;
	int* arr;
};

struct que* createque(unsigned atualSize){
	struct que* que = (struct que*)malloc(sizeof(struct que));
	que->actualSize = atualSize;
	que->size = 0;
	que->front = que->rear = -1;
	que->arr = (int*)malloc(que->actualSize * sizeof(int));
	return que;
}

int isFull(struct que* que){
	return (que->size == que->actualSize);
}

void enqueue(struct que* que, int item){
	if (isFull(que))
		return;
	que->rear = que->rear + 1;
	que->arr[que->rear] = item;
	if(que->front==-1)
		que->front++;
	
	printf("%d adicionadoa fila \n", item);
}

int isEmpty(struct que* que){
	return (que->size == 0);
}

int dequeue(struct que* que){
	if (isEmpty(que))
		return INT_MIN;
	int item = que->arr[que->front];
	if(que->front==que->rear){
		que->front = que->rear = -1;
	}else{
		que->front = que->front + 1;
	}
	que->size = que->size - 1;
	return item;		
}

int front(struct que* que){
	if(isEmpty(que))
		return INT_MIN;
		
	return que->arr[que->front];
}

int rear(struct que* que){
	if(isEmpty(que))
		return INT_MIN;
		
	return que->arr[que->rear];
}

int main(){
	int val,n;
	struct que* que = createque(1000);
	do{
		printf("\n1. ENQUEUE");
		printf("\n2. DEQUEUE");
		printf("\n3. IS EMPTY");
		printf("\n4. IS FULL");
		printf("\n5. FIRST ELE");
		printf("\n6. LAST ELE");
		printf("\n7. SAIR \n");
		scanf("%d", &n);
		switch(n){
			case 1: 
				printf("\nValor: ");
				scanf("%d", &val);
				enqueue(que, val);
				break;
			case 2:
				dequeue(que);
				break;
			case 3:
				printf("\n Vaziu? : %d", isEmpty(que));
				break;
			case 4:
				printf("\n Cheio? : %d", isFull(que));
				break;
			case 5:
				printf("\n Inicio : %d", front(que));
				break;
			case 6:
				printf("\n Fim : %d", rear(que));
				break;
			case 7: 
				exit(0);
				break;
			default: printf("\n INVALIDO!");	
		}
		printf("\n Continuar?... ");
	}while('y'==getch());
}
