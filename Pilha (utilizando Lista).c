#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
} *p, *tmp, *tmp1, *end;

void insert(int);
void display();
void delete();
void isEmpty();


int main() {
	int val, n;
	p = NULL;
	
	do{
		printf("\n1. PUSH");
		printf("\n2. POP");
		printf("\n3. IS EMPTY");
		printf("\n4. Display");
		printf("\n5. SAIR \n");
		scanf("%d", &n);
		
		switch(n){
			case 1: 
				printf("\nValor: ");
				scanf("%d", &val);
				insert(val);
				break;
			case 2:
				delete();
				break;
			case 3:
				isEmpty();
				break;
			case 4:
				display();
				break;
			case 5: 
				exit(0);
				break;
			default: printf("\n INVALIDO!");	
		}	
		printf("\n Quer continuar? (y/n) ");				
	}while('y' == getch());
	
	return 0;
}


void insert(int ele){
	tmp = (struct node*)malloc(sizeof(struct node));
	tmp->data = ele;
	if(p == NULL){
		tmp->next = NULL;
	}else{
			tmp->next = p;
	}	
	p = tmp;	
}


void delete(){
	tmp=p;
	if(p==NULL){ printf("\n Nenhum elemento!! "); }
	else{
		printf("\nelemento %d apagado.", p->data);
		p=p->next;
	}
}

void isEmpty() {
	if(p==NULL){
		printf("Pilha esta Vazio!");
	}else{
		printf("A Pilha nao esta vazio");
	}
}


void display(){
	tmp=p;
	while(tmp!=NULL){
		printf("\n %d", tmp->data);
		tmp=tmp->next;
	}
}

