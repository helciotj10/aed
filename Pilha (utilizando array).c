#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct stackk {
	int top;
	unsigned size;
	int* array;
};

struct stackk* create(unsigned size) {
	struct stackk* stackk = (struct stackk*)malloc(sizeof(struct stackk));
	stackk->size = size;
	stackk->top = -1;
	stackk->array = (int*)malloc(stackk->size * sizeof(int));
	return stackk;
}

int isFull(struct stackk* stackk){
	return stackk->top == stackk->size - 1;
}

int isEmpty(struct stackk* stackk){
	return stackk->top == -1;
}

void push(struct stackk* stackk, int item){
	if(isFull(stackk))
		return;
	
	stackk->array[++stackk->top] = item;
}

int pop(struct stackk* stackk){
	if(isEmpty(stackk))
		return -1;
		
	return stackk->array[stackk->top--];
}

int peek(struct stackk* stackk){
	if(isEmpty(stackk))
		return INT_MIN;
	
	return stackk->array[stackk->top];
}

int main(){
	int val,n;
	struct stackk* stackk = create(100);
	do{
		printf("\n1. PUSH");
		printf("\n2. POP");
		printf("\n3. PEEK");
		printf("\n4. IS EMPTY");
		printf("\n5. SAIR \n");
		scanf("%d", &n);
		switch(n){
			case 1: 
				printf("\nValor: ");
				scanf("%d", &val);
				push(stackk, val);
				break;
			case 2:
				printf("\n Pop elemento : %d", pop(stackk));
				break;
			case 3:
				printf("\n elemento no topo : %d", peek(stackk));
				break;
			case 4:
				printf("\n Vaziu? : %d", isEmpty(stackk));
				break;
			case 5: 
				exit(0);
				break;
			default: printf("\n INVALIDO!");	
		}
		printf("\n Continuar?... ");
	}while('y'==getch());
}

