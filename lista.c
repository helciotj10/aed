#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
} *p, *tmp, *tmp1;

void insert_end(int);
void insert_beg(int);
void delete_beg();
void delete_end();
void display();

int main() {
	int val, n;
	p = NULL;
	
	do{
		printf("\n1.Inserir no fim");
		printf("\n2.Inserir no Inicio");
		printf("\n4.Apagar no inicio");	
		printf("\n5.Apagar no fim");
		printf("\n6.Mostrar");
		printf("\n7.Sair \n");
		scanf("%d", &n);
		
		switch(n){
			case 1: 
			        printf("\n Valor:");
			        scanf("%d", &val);
			        insert_end(val);
			        break;
			case 2:
				    printf("\n Valor:");
			        scanf("%d", &val);
			        insert_beg(val);
			        break;
			case 4: 
					delete_beg();
					break;
			case 5:
					delete_end();
					break;
			case 6:
				    display();
				    break;
			case 7:
				    exit(0);
				    break;
			default:
				    printf("\n Invalido!");
				    break;		          
		}	
		printf("\n Quer continuar? (y/n) ");				
	}while('y' == getch());
	
	return 0;
}


void insert_end(int ele){
	tmp = p;
	tmp1=(struct node*)malloc(sizeof(struct node));
	tmp1->data = ele;
	tmp1->next = NULL;
	
	if(p==NULL){ p=tmp1; }
	else {
		while(tmp->next != NULL){
			tmp = tmp->next;
		}
		tmp->next = tmp1;
	}	
}

void insert_beg(int ele){
	tmp = p;
	tmp1 = (struct node*)malloc(sizeof(struct node));
	tmp1->data = ele;
	tmp1->next =p;
	p = tmp1;
}

void delete_beg(){
	tmp=p;
	if(p==NULL){ printf("\n Nenhum elemento!! "); }
	else{
		printf("\nelemento %d apagado.", p->data);
		p=p->next;
	}
}

void delete_end(){
	tmp=p;
	struct node* pre;
	if(p==NULL){ printf("\n Nenhum elemento!! "); }
	else if(p->next == NULL){
		printf("\nelemento %d apagado.", p->data);
		p=NULL;
	}else{
		while(tmp->next!=NULL){
			pre=tmp;
			tmp=tmp->next;
		}
		pre->next=NULL;
		printf("\nelemento %d apagado.", tmp->data);
	}
}

void display(){
	tmp=p;
	while(tmp!=NULL){
		printf("\n %d", tmp->data);
		tmp=tmp->next;
	}
}

